// 1. Описать своими словами для чего вообще нужны функции в программировании.
//     Функции нужны, чтобы не повторять один и тот же код многократно. Описывается инструкция, а потом используется как шаблон в разных контекстах.
// 2. Описать своими словами, зачем в функцию передавать аргумент.
//     Функции что-то принимают на входе и что-то выдают в результате. Чтобы принять данные на входе и нужен аргумент.
// 3. Что такое оператор return и как он работает внутри функции?
//    Return - команда, которая действует внутри функции и прирывает работу функции и возвращает результат, если он описан после return.




const getNumber = (message) => {
	let userNumber;
	
	do {userNumber = prompt(message, userNumber)}	
	while(isNaN(+userNumber) || userNumber === '')
	// while(isNaN(+userNumber) && userNumber !== '' && userNumber !== null)
	
	return +userNumber;
}



const calcPlus = () => {
    const firstNumber = getNumber("Введите первое число");
    const secondNumber = getNumber("Введите второе число");
    return firstNumber + secondNumber
}

const calcMinus = () => {
    const firstNumber = getNumber("Введите первое число");
    const secondNumber = getNumber("Введите второе число");
    return firstNumber - secondNumber
}

const calcMultiply = () => {
    const firstNumber = getNumber("Введите первое число");
    const secondNumber = getNumber("Введите второе число");
    return firstNumber * secondNumber
}

const calcDivide = () => {
    const firstNumber = getNumber("Введите первое число");
    const secondNumber = getNumber("Введите второе число");
    return firstNumber / secondNumber
}

const getMathThings = () => {
	let userChoice;
	
	do	 {userChoice = prompt('Введите  + , - , *, / ',userChoice)}

	  while(userChoice !== '+' && userChoice !== '-' && userChoice !== "*" && userChoice !=="/")
    
	  return userChoice;
}



const calculateResult = () => {
	const userChoice = getMathThings();

	switch(userChoice) {
		case "+":
			console.log(calcPlus());
			break;
		case "_":
			console.log(calcMinus());
			break;
		case "*":
			console.log(calcMultiply());
			break;
		case "/":
			console.log(calcDivide());
			break
	}
}
calculateResult()

